import csv
import array 
import _thread
import threading
import time
from bs4 import BeautifulSoup
from operator import itemgetter
from builtins import int
import requests
from _thread import start_new_thread
from asyncio.tasks import wait
#beautifulSoup parsing from rotowire.com

#spoof a set of currentPlayers for testing purposes (when lineup cards are not available)
useTodaysLineups = 0
#0 = original, 1 = two pitchers threaded
functionFlag = 2

startTime = time.time()

url = "rotowire.com/baseball/daily_lineups.htm"

r = requests.get("http://" + url)

data = r.text

soup = BeautifulSoup(data, "html.parser")

results = soup.findAll("div", class_="dlineups-vplayer")
results2 = soup.findAll("div", class_="dlineups-hplayer")
results3 = soup.findAll("div", class_="span11 dlineups-pitchers")

currentPlayers = list()

#traverse the results and append all the current players for lineups into the checker
for x in results:
    for y in x.find_all('a'):
        currentPlayers.append(y.get('title'))   
for x in results2:
    for y in x.find_all('a'):
        currentPlayers.append(y.get('title'))   
        
#traverse and find all starting pitchers
for x in results3:
    for y in x.findAll('a'):
        #print(y.get_text())
        currentPlayers.append(y.get_text())

#print(currentPlayers.__len__(), currentPlayers)

with open('50PlayerTest.csv', newline='') as csvfile:
    DKreader = csv.reader(csvfile)
    DKlist = list()
    for row in DKreader:
        l = list()
        i = 1
        #print(row)
        for column in row:
            #print(column, i)
            if i == 1:
                if column == 'SP':
                    l.append(1.0)
                elif column == 'RP':
                    l.append(1.1)
                elif column == 'C':
                    l.append(2.0)
                elif column == 'C/OF':
                    l.append(2.7)
                elif column == 'C/1B':
                    l.append(2.3)
                elif column == 'C/2B':
                    l.append(2.4)
                elif column == 'C/3B':
                    l.append(2.5)
                elif column == 'C/SS':
                    l.append(2.6)
                elif column == '1B':
                    l.append(3.0)
                elif column == '1B/2B':
                    l.append(3.4)
                elif column == '1B/3B':
                    l.append(3.5)
                elif column == '1B/C':
                    l.append(3.2)
                elif column == '1B/OF':
                    l.append(3.7)
                elif column == '1B/SS':
                    l.append(3.6)
                elif column == '2B':
                    l.append(4.0)
                elif column == '2B/3B':
                    l.append(4.5)
                elif column == '2B/C':
                    l.append(4.2)
                elif column == '2B/OF':
                    l.append(4.7)
                elif column == '2B/SS':
                    l.append(4.6)
                elif column == '3B':
                    l.append(5.0)
                elif column == '3B/C':
                    l.append(5.2)
                elif column == '3B/OF':
                    l.append(5.7)
                elif column == '3B/SS':
                    l.append(5.6)
                elif column == 'OF':
                    l.append(7.0);
                elif column == 'OF/SS':
                    l.append(7.6)
                elif column == 'SS':
                    l.append(6.0)
            elif i == 2 and column != 'Name':
                l.append(column)
            elif i == 3:
                if column != 'Salary':
                    l.append(int(column))
            elif i == 5:
                if column != 'AvgPointsPerGame':
                    #print(l)
                    l.append(float(column))
                    #print(l)
                    l.append(l[3] / l[2])
            i = i + 1
        DKlist.append(l)

DKlist.pop(0)

# begin creation of array of lists holding each position's players
SP = list()
C = list()
oneB = list()
twoB = list()
threeB = list()
SS = list()
OF = list()

# boolean for popping list
i = 1
while i == 1:
    if DKlist.__len__() == 0:
        i = 2
    else:
        current = DKlist.pop()
        if useTodaysLineups == 0:
            if current[3] >= 1.0 and current[4] <= 0.0032 and current[4] >= 0.0014:             
                    if current[0] == 4.0:
                        twoB.append(current)
                    elif current[0] == 4.2:
                        C.append(current)
                        twoB.append(current)
                    elif current[0] == 4.5:
                        twoB.append(current)
                        threeB.append(current)
                    elif current[0] == 4.6:
                        twoB.append(current)
                        SS.append(current)
                    elif current[0] == 4.7:
                        twoB.append(current)
                        OF.append(current)
                    elif current[0] == 3.0:
                        oneB.append(current)
                    elif current[0] == 3.2:
                        oneB.append(current)
                        C.append(current) 
                    elif current[0] == 3.4:
                        oneB.append(current)
                        twoB.append(current)
                    elif current[0] == 3.5:
                        oneB.append(current)
                        threeB.append(current)
                    elif current[0] == 3.7:
                        oneB.append(current)
                        OF.append(current)
                    elif current[0] == 3.6:
                        oneB.append(current)
                        SS.append(current)
                    elif current[0] == 5.0:
                        threeB.append(current)        
                    elif current[0] == 5.2:
                        threeB.append(current)
                        C.append(current)
                    elif current[0] == 5.6:
                        threeB.append(current)
                        SS.append(current)
                    elif current[0] == 5.7:
                        threeB.append(current)
                        OF.append(current)
                    elif current[0] == 7.0:
                        OF.append(current)
                    elif current[0] == 7.6:
                        OF.append(current)
                        SS.append(current)
                    elif current[0] == 6.0:
                        SS.append(current)
                    elif current[0] == 1.0:
                        SP.append(current)
                    elif current[0] == 2.0:
                        C.append(current)
                    elif current[0] == 2.3:
                        C.append(current)
                    elif current[0] == 2.4:
                        C.append(current)
                    elif current[0] == 2.5:
                        C.append(current)
                    elif current[0] == 2.6:
                        C.append(current)
                    elif current[0] == 2.7:
                        C.append(current)                                
                    else: 
                        i = 1
        else:
            for x in currentPlayers:
                if x == current[1]:
                    if current[3] >= 1.0 and current[4] <= 0.0035:             
                        if current[0] == 4.0:
                            twoB.append(current)
                        elif current[0] == 4.2:
                            C.append(current)
                            twoB.append(current)
                        elif current[0] == 4.5:
                            twoB.append(current)
                            threeB.append(current)
                        elif current[0] == 4.6:
                            twoB.append(current)
                            SS.append(current)
                        elif current[0] == 4.7:
                            twoB.append(current)
                            OF.append(current)
                        elif current[0] == 3.0:
                            oneB.append(current)
                        elif current[0] == 3.2:
                            oneB.append(current)
                            C.append(current) 
                        elif current[0] == 3.4:
                            oneB.append(current)
                            twoB.append(current)
                        elif current[0] == 3.5:
                            oneB.append(current)
                            threeB.append(current)
                        elif current[0] == 3.7:
                            oneB.append(current)
                            OF.append(current)
                        elif current[0] == 3.6:
                            oneB.append(current)
                            SS.append(current)
                        elif current[0] == 5.0:
                            threeB.append(current)        
                        elif current[0] == 5.2:
                            threeB.append(current)
                            C.append(current)
                        elif current[0] == 5.6:
                            threeB.append(current)
                            SS.append(current)
                        elif current[0] == 5.7:
                            threeB.append(current)
                            OF.append(current)
                        elif current[0] == 7.0:
                            OF.append(current)
                        elif current[0] == 7.6:
                            OF.append(current)
                            SS.append(current)
                        elif current[0] == 6.0:
                            SS.append(current)
                        elif current[0] == 1.0:
                            alreadyInFlag = 0
                            for X in SP:
                                if X == current:
                                    alreadyInFlag = 1
                            if alreadyInFlag == 0:
                                SP.append(current)
                        elif current[0] == 2.0:
                            C.append(current)
                        elif current[0] == 2.3:
                            C.append(current)
                        elif current[0] == 2.4:
                            C.append(current)
                        elif current[0] == 2.5:
                            C.append(current)
                        elif current[0] == 2.6:
                            C.append(current)
                        elif current[0] == 2.7:
                            C.append(current)                                
                        else: 
                            i = 1
            
#print(DKlist)   
#print(SS)    

SP.sort(key=itemgetter(3), reverse=True)
C.sort(key=itemgetter(4), reverse=True)
OF.sort(key=itemgetter(4), reverse=True)
threeB.sort(key=itemgetter(4), reverse=True)
twoB.sort(key=itemgetter(4), reverse=True)
oneB.sort(key=itemgetter(4), reverse=True)
SS.sort(key=itemgetter(4), reverse=True)

def cutListTop20(L, length):
    i = 1
    #print(length)
    #print(L)
    while i <= (length*0.8):
        #print(L.pop())
        i = i+1
        
#cutListTop20(SP, SP.__len__())
#cutListTop20(C, C.__len__())
#cutListTop20(OF, OF.__len__())
#cutListTop20(threeB, threeB.__len__())
#cutListTop20(twoB, twoB.__len__())
#cutListTop20(oneB, oneB.__len__())
#cutListTop20(SS, SS.__len__())

#print(SP.__len__(), SP)
#print(OF.__len__(), OF)
#print(C.__len__(), C)
#print(oneB.__len__(), oneB)
#print(twoB.__len__(), twoB)
#print(threeB.__len__(), threeB)
#print(SS.__len__(), SS)

# container for brute forced lineup cards
bruteLineups = list()
# begin brute force

def refinedBattersOnly(SPone, SPtwo):
    highScore = 0
    for Cx in C:
        for oneBx in oneB:
            for twoBx in twoB:
                #if oneBx == Cx: exit
                for threeBx in threeB:
                    #if twoBx == oneBx or twoBx == Cx: exit
                    for SSx in SS:
                        #if threeBx == twoBx or threeBx == oneBx or threeBx == Cx: exit
                        x = 1
                        for OFx in OF:
                            #if SSx == threeBx or SSx == twoBx or SSx == oneBx or SSx == Cx: exit
                            y = 1
                            z = 1
                            for OFy in OF:
                                #if OFx == SSx or OFx == threeBx or OFx == twoBx or OFx == oneBx or OFx == Cx: exit
                                z = 1
                                if y > x:
                                    for OFz in OF:
                                        #if OFy == SSx or OFy == threeBx or OFy == twoBx or OFy == oneBx or OFy == Cx: exit
                                        #print("made it to OFz")
                                        if z > y:
                                            #run best lineup checking code
                                            totalSalary = Cx[2] + oneBx[2] + twoBx[2] + threeBx[2] + SSx[2] + OFx[2] + OFy[2] + OFz[2] + SPone[2] + SPtwo[2]
                                            totalPoints = Cx[3] + oneBx[3] + twoBx[3] + threeBx[3] + SSx[3] + OFx[3] + OFy[3] + OFz[3] + SPone[3] + SPtwo[3]
                                            #print(x, y, z)
                                            #if totalSalary <= 50000 and totalPoints > highScore and OFz != SSx and OFz != threeBx and OFz != twoBx and OFz != oneBx and OFz != Cx:
                                            if totalSalary <= 50000 and totalPoints > highScore: 
                                                #print("made it")
                                                lineup = list()
                                                lineup.append(totalSalary)
                                                lineup.append(totalPoints)
                                                lineup.append(SPone)
                                                lineup.append(SPtwo)
                                                lineup.append(Cx)
                                                lineup.append(oneBx)
                                                lineup.append(twoBx)
                                                lineup.append(threeBx)
                                                lineup.append(SSx)
                                                lineup.append(OFx)
                                                lineup.append(OFy)
                                                lineup.append(OFz)
                                                #check below for same name
#                                                bruteLineups.append(lineup)
#                                                highScore = totalPoints
                                                i = 0
                                                while i <= 11:
                                                    if i >= 2:
                                                        count = lineup.count(lineup[i][1])
                                                        #print(lineup[i][1])
                                                        if count > 1:
                                                            exit
                                                        elif i == 11:
                                                            highScore = totalPoints 
                                                            bruteLineups.append(lineup)
                                                    i = i + 1
                                        z = z + 1
                                y = y + 1
                            x = x + 1       

def threadedAlgo(catcher):
    #bruteLineups = list()
    # begin brute force
    
    comboCount = 0
    bestScore = 0.0
    bestLineup = list()
    newLineup = list()
    newLineup.append(0)
    newLineup.append(0.0)
    #newLineup.append(catcher)
    intA = 0
    intaa = 0
    intB = 0
    intfff = 0
    intg = 0
    salaryCatcher = catcher[2]
    ptsCatcher = catcher[3]
    for A in OF:
        if A != catcher:
            #print('base loop')
            salaryA = 0
            ptsA = 0.0
            intA = intA + 1
            intaa = 0
            intB = 0
            intfff = 0
            intg = 0
            #newLineup.append(A)
            #salary total
            salaryA = salaryA + A[2]
            #newLineup[0] = newLineup[0] + A[2]
            #avgPoints total
            ptsA = ptsA + A[3]
            #newLineup[1] = newLineup[1] + A[3]
        #   this will be the code used for the last loop bruteLineups.append(newLineup)
            for aa in OF:
                intaa = intaa + 1
                if (intaa > intA) and aa != A and aa != catcher:
                    salaryAA = 0
                    ptsAA = 0.0
                    #newLineup.pop()
                    #newLineup.append(aa)
                    salaryAA = salaryAA + aa[2]
                    ptsAA = ptsAA + aa[3]
                    intB = 0
                    intfff = 0
                    intg = 0
                    for B in OF:
                        intB = intB + 1
                        intfff = 0
                        intg = 0
                        if ((intB > intA) and (intB > intaa)) and B != aa and B != A and B != catcher:
                        #if B != aa and B != A and B != catcher:
                            salaryB = 0
                            ptsB = 0.0
                            #newLineup.pop()
                            #newLineup.append(B)
                            salaryB = salaryB + B[2]
                            ptsB = ptsB + B[3]
                            for cc in oneB:
                                if cc != B and cc != aa and cc != A and cc != catcher:
                                    salaryCC = 0
                                    ptsCC = 0.0
                                    #newLineup.pop()
                                    #newLineup.append(cc)
                                    salaryCC = salaryCC + cc[2]
                                    ptsCC = ptsCC + cc[3] 
                                    for d in twoB:
                                        if d != cc and d != B and d != aa and d != A and d != catcher:
                                            salaryD = 0
                                            ptsD = 0.0
                                            #newLineup.pop()
                                            #newLineup.append(d)
                                            salaryD = salaryD + d[2]
                                            ptsD = ptsD + d[3]                   
                                            for e in threeB:
                                                if e != d and e != cc and e != B and e != aa and e != A and e != catcher:
                                                    salaryE = 0
                                                    ptsE = 0.0
                                                    #newLineup.pop()
                                                    #newLineup.append(e)
                                                    salaryE = salaryE + e[2]
                                                    ptsE = ptsE + e[3] 
                                                    for f in SS:
                                                        if f != e and f != d and f != cc and f != B and f != aa and f != A and f != catcher:
                                                            salaryF = 0
                                                            ptsF = 0.0
                                                            #newLineup.pop()
                                                            #newLineup.append(f)
                                                            salaryF = salaryF + f[2]
                                                            ptsF = ptsF + f[3]
                                                            intfff = 0
                                                            intg = 0
                                                            for fff in SP:
                                                                intfff = intfff + 1
                                                                intg = 0
                                                                if fff != f and fff != e and fff != d and fff != cc and fff != B and fff != aa and fff != A:
                                                                    salaryFFF = 0
                                                                    ptsFFF = 0.0
                                                                    #newLineup.pop()
                                                                    #newLineup.append(fff)
                                                                    salaryFFF = salaryFFF + fff[2]
                                                                    ptsFFF = ptsFFF + fff[3]                              
                                                                    for g in SP:
                                                                        intg = intg + 1
                                                                        if (intg > intfff) and g != fff and g != f and g != e and g != d and g != cc and g != B and g != aa and g != A:
                                                                            salaryG = 0
                                                                            ptsG = 0.0
                                                                            #newLineup.append(g)
                                                                            salaryG = salaryG + g[2]
                                                                            ptsG = ptsG + g[3]
                                                                            salaryTotal = 0
                                                                            salaryTotal = salaryA + salaryAA + salaryB + salaryCC + salaryD + salaryE + salaryF + salaryFFF + salaryG + salaryCatcher
                                                                            ptsTotal = ptsA + ptsB + ptsCC + ptsAA + ptsD + ptsE + ptsF + ptsFFF + ptsG + ptsCatcher
                                                                            if salaryTotal <= 50000 and ptsTotal > bestScore: # and points are greater than previous entry
                                                                                bestLineup = list()
                                                                                bestLineup.append(salaryTotal)
                                                                                bestLineup.append(ptsTotal)
                                                                                bestLineup.append(catcher)
                                                                                bestLineup.append(A)
                                                                                bestLineup.append(aa)
                                                                                bestLineup.append(f)
                                                                                bestLineup.append(e)
                                                                                bestLineup.append(fff)
                                                                                bestLineup.append(g)
                                                                                bestLineup.append(B)
                                                                                bestLineup.append(cc)
                                                                                bestLineup.append(d)
                                                                                bestScore = ptsTotal
                                                                                comboCount = comboCount + 1
                                                                                #print(comboCount, bestScore, bestLineup)
                                                                            #newLineup.pop()
                                                                    #newLineup.pop()
                                                            #newLineup.pop()       
                                                    #newLineup.pop()        
                                            #newLineup.pop()        
                                    #newLineup.pop()
                            #newLineup.pop()        
                    #newLineup.pop()                       
            #newLineup.pop() 
    bruteLineups.append(bestLineup)
    
def battersOnly(SPone, SPtwo):
    #bruteLineups = list()
    # begin brute force
    
    comboCount = 0
    bestScore = 0.0
    bestLineup = list()
    newLineup = list()
    newLineup.append(0)
    newLineup.append(0.0)
    #newLineup.append(catcher)
    intA = 0
    intaa = 0
    intB = 0
    intfff = 0
    salaryCatcher = SPone[2] + SPtwo[2]
    ptsCatcher = SPone[3] + SPtwo[3]
    for A in OF:
        if A != SPone:
            #print('base loop', SPone, SPtwo)
            salaryA = 0
            ptsA = 0.0
            intA = intA + 1
            intaa = 0
            intB = 0
            intfff = 0
            #newLineup.append(A)
            #check to see if salary will be over 50k with this player
            #salary total
            salaryA = salaryA + A[2]
            #newLineup[0] = newLineup[0] + A[2]
            #avgPoints total
            ptsA = ptsA + A[3]
            #newLineup[1] = newLineup[1] + A[3]
        #   this will be the code used for the last loop bruteLineups.append(newLineup)
            for aa in OF:
                #print('second loop')
                intaa = intaa + 1
                if (intaa > intA) and aa != A:
                    salaryAA = 0
                    ptsAA = 0.0
                    #newLineup.pop()
                    #newLineup.append(aa)
                    salaryAA = salaryAA + aa[2]
                    ptsAA = ptsAA + aa[3]
                    intB = 0
                    intfff = 0
                    for B in OF:
                        intB = intB + 1
                        intfff = 0
                        if ((intB > intA) and (intB > intaa)) and B != aa and B != A:
                        #if B != aa and B != A and B != catcher:
                            salaryB = 0
                            ptsB = 0.0
                            #newLineup.pop()
                            #newLineup.append(B)
                            salaryB = salaryB + B[2]
                            ptsB = ptsB + B[3]
                            for cc in oneB:
                                if cc != B and cc != aa and cc != A:
                                    salaryCC = 0
                                    ptsCC = 0.0
                                    #newLineup.pop()
                                    #newLineup.append(cc)
                                    salaryCC = salaryCC + cc[2]
                                    ptsCC = ptsCC + cc[3] 
                                    for d in twoB:
                                        if d != cc and d != B and d != aa and d != A:
                                            salaryD = 0
                                            ptsD = 0.0
                                            #newLineup.pop()
                                            #newLineup.append(d)
                                            salaryD = salaryD + d[2]
                                            ptsD = ptsD + d[3]                   
                                            for e in threeB:
                                                if e != d and e != cc and e != B and e != aa and e != A:
                                                    salaryE = 0
                                                    ptsE = 0.0
                                                    #newLineup.pop()
                                                    #newLineup.append(e)
                                                    salaryE = salaryE + e[2]
                                                    ptsE = ptsE + e[3] 
                                                    for f in SS:
                                                        if f != e and f != d and f != cc and f != B and f != aa and f != A:
                                                            salaryF = 0
                                                            ptsF = 0.0
                                                            #newLineup.pop()
                                                            #newLineup.append(f)
                                                            salaryF = salaryF + f[2]
                                                            ptsF = ptsF + f[3]
                                                            intfff = 0
                                                            for fff in C:
                                                                intfff = intfff + 1
                                                                if fff != f and fff != e and fff != d and fff != cc and fff != B and fff != aa and fff != A:
                                                                    salaryFFF = 0
                                                                    ptsFFF = 0.0
                                                                    #newLineup.pop()
                                                                    #newLineup.append(fff)
                                                                    salaryFFF = salaryFFF + fff[2]
                                                                    ptsFFF = ptsFFF + fff[3]   
                                                                    salaryTotal = 0
                                                                    salaryTotal = salaryA + salaryAA + salaryB + salaryCC + salaryD + salaryE + salaryF + salaryFFF + salaryCatcher
                                                                    ptsTotal = ptsA + ptsB + ptsCC + ptsAA + ptsD + ptsE + ptsF + ptsCatcher + ptsFFF
                                                                    if salaryTotal <= 50000 and ptsTotal > bestScore: # and points are greater than previous entry
                                                                        bestLineup = list()
                                                                        bestLineup.append(salaryTotal)
                                                                        bestLineup.append(ptsTotal)
                                                                        bestLineup.append(SPone)
                                                                        bestLineup.append(SPtwo)
                                                                        bestLineup.append(A)
                                                                        bestLineup.append(aa)
                                                                        bestLineup.append(f)
                                                                        bestLineup.append(e)
                                                                        bestLineup.append(B)
                                                                        bestLineup.append(cc)
                                                                        bestLineup.append(d)
                                                                        bestLineup.append(fff)
                                                                        bestScore = ptsTotal
                                                                        comboCount = comboCount + 1
                                                                        #print(comboCount, bestScore, bestLineup)

                                                            #newLineup.pop()       
                                                    #newLineup.pop()        
                                            #newLineup.pop()        
                                    #newLineup.pop()
                            #newLineup.pop()        
                    #newLineup.pop()                       
            #newLineup.pop() 
    bruteLineups.append(bestLineup)    

    
threads = []

if functionFlag == 0:   
    for X in SP:
        #print(X)
        t = threading.Thread(target=threadedAlgo, args=(X,))
        threads.append(t)
        t.start()
elif functionFlag == 1:
    Xflag = 0
    for X in SP:
        Xflag = Xflag + 1
        if Xflag <= 6:
            Yflag = 0
            for Y in SP:
                Yflag = Yflag + 1
                if Yflag <=6 and Yflag > Xflag and Y != X:
                    #print(X, Y)
                    #print(X)
                    t = threading.Thread(target=battersOnly, args=(X,Y,))
                    threads.append(t)
                    t.start()
elif functionFlag == 2:
    Xflag = 0
    for X in SP:
        Xflag = Xflag + 1
        if Xflag <= 6:
            Yflag = 0
            for Y in SP:
                Yflag = Yflag + 1
                if Yflag <=6 and Yflag > Xflag and Y != X:
                    #print(X, Y)
                    #print(X)
                    t = threading.Thread(target=refinedBattersOnly, args=(X,Y,))
                    threads.append(t)
                    t.start()
   
while threading.activeCount() > 1:
    wait

bruteLineups.sort(key=itemgetter(1), reverse=True)
print('')
print('Best Lineup:')
print(bruteLineups[0])
endTime = time.time()
totalTime = endTime - startTime
playerSum = SP.__len__() + SS.__len__() + oneB.__len__() + twoB.__len__() + threeB.__len__() + OF.__len__() + C.__len__()
print('Position Players: ', playerSum)
print('Runtime: ', totalTime)
